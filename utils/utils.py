#This file has functions that are helpful to me!

#This function will generate random numbers in the correct range so that particle position can be random.
#Will return a randomized position that includes x, y , and z coordinates.
def randNumGen(L):
    import random
    toprange = L / 2
    bottomrange = 0 - toprange
    xcoord = random.uniform(toprange, bottomrange)
    ycoord = random.uniform(toprange, bottomrange)
    zcoord = random.uniform(toprange, bottomrange)
    return [xcoord,ycoord,zcoord]

#This function will calculate the number of particles needed to make the specified density in a box 
#specfied by the user.
def ParticleCalc(D,Lx):
    volume = Lx * Lx * Lx
    numParticles = round(volume * D, 0)
    numParticles = int(numParticles)
    return numParticles

#This function will generate random numbers in the correct range for random particle positions but it attempts
#to do it so that the box is 2D which is why the zcoord is equal to 1. 
def NumGenWith2D(L):
    import random
    toprange = L/2
    bottomrange = 0 - toprange
    xcoord = random.uniform(toprange, bottomrange)
    ycoord = random.uniform(toprange, bottomrange)
    zcoord = 1
    return [xcoord,ycoord,zcoord]
