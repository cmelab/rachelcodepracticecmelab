How to Set up a Macbook for the Lab!
(or at least some ideas)

Should make sure you have python too before doing a lot!

Install Homebrew Using:
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

"brew doctor" is a good command to use to check and see what is going wrong

Installing Packages with Homebrew:
brew install git (if it isn't working, update homebrew using "brew upgrade")
brew install cmake
brew install build-essential
brew install rlwrap
brew install wget

Clone the getting-started and cme-utils packages from Bitbucket

Setting up VMD/gsd:
Make sure you have downloaded VMD from the internet (you have to make an account)
git clone https://github.com/mphoward/gsd-vmd
cd gsd-vmd
mkdir build && cd build
cmake ..
make install
(I can't remember if we needed all of these but this was the sequence of commands)

Since I was moving from one computer to another (I think this is why), we ran these other commands:
cp bash_profile ~/.bash_profile
source .bash_profile
cp vmdrc ~/.vmdrc
cp vimrc ~/.vimrc

Downloading MiniConda:
Download from the internet first
cd Downloads
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh
conda source

There are some repeats but this is what I ran in the environment I created:
*conda install -c mosdef -c omnia mbuild foyer --only-deps
*conda install -c conda-forge -c mosdef -c omnia hoomd jupyter numpy matplotlib freud gsd

Installing Signac/Signac Flow:
conda install -c conda-forge signac signac-flow

Environment Commands:
conda activate <environment name> --> puts you in your environment
conda deactivate --> takes you out of environment
conda env remove -n <environment name> --> removes environment
conda create --name <new environment name> --clone <the one you are cloning> --> copies one environment into another

Also download Docker and Mendeley for easy access. 
DO NOT USE PIP TO INSTALL MBUILD

