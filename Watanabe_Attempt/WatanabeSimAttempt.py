#This code (hopefully) will run a simulation that is similar to the one in the paper "Phase diagram and universality
#of the Lennard-Jones gas-liquid system" by Hiroshi Watanabe, Nobuyasu Ito, and Chin-Kun Hu.

#Some of the parameters weren't available in the paper for the way my code was set up so some (like tau) were decided
#based on examples from the HOOMD documentation.

#Hopefully this all works!

import hoomd
import hoomd.md
import numpy
import utils

hoomd.context.initialize("");

#In the paper they specified this box size
Lx = 64
a = hoomd.data.boxdim(Lx = Lx, Ly = Lx, Lz = 128)

#I created a snapshot with the number of specified particles and instead of trying to do liquid and gas, I focused on 
#just one particle and didn't worry about the bonds between them.
snapshot = hoomd.data.make_snapshot(N=180288, box = a,
                                   particle_types=['A'])
N = snapshot.particles.N

#This loop places all the particles randomly in the box
for i in range(N):
    snapshot.particles.position[i] = utils.randNumGen(Lx)
print(snapshot.particles.position)

#Gives all the particles a velocity - I didn't make it uniform because I didn't think that was realistic (?)
my_velocity = numpy.random.random((N,3)) * 2 - 1
snapshot.particles.velocity[:] = my_velocity[:]

#Since all the particles are the same, I gave them all the same mass.
snapshot.particles.mass[:] = 1.0

#Reads the snapshot and should print out the number of particles I am working with. 
hoomd.init.read_snapshot(snapshot)

#Using the cell method for the neighborlist    
nl = hoomd.md.nlist.cell()

#The paper specified this r_cut value
lj = hoomd.md.pair.lj(r_cut=3.0, nlist=nl)

#Epsilon and sigma were decided from a HOOMD example    
lj.pair_coeff.set('A', 'A', epsilon=1.0, sigma=1.0)

#The paper specified this dt
hoomd.md.integrate.mode_standard(0.005)

#The paper specified this kT and tau was decided through a HOOMD example    
all = hoomd.group.all()
hoomd.md.integrate.nvt(group = all, kT = 0.9, tau=0.5)

hoomd.analyze.log(filename = "WatanabeSim-output.log",
                  quantities=['potential_energy', 'temperature'],
                  period = 100,
                  overwrite = True)

hoomd.dump.gsd("WatanabeSimOutputs.gsd", period=2.5e5, group=all, overwrite = True)

#One of the simulations in the paper ran for this many time steps so I decided to use that same number.
hoomd.run(5000000)

#Plotting the data obtained in the simulation run.    
from matplotlib import pyplot
data = numpy.genfromtxt(fname='WatanabeSim-output.log', skip_header=True)

pyplot.figure(figsize=(10,5), dpi=140);
pyplot.plot(data[:,0], data[:,1]);
pyplot.xlabel('time step');
pyplot.ylabel('potential_energy');
pyplot.savefig("PEWatanabeSim.png")

pyplot.figure(figsize=(10,5), dpi=140);
pyplot.plot(data[:,0], data[:,2]);
pyplot.xlabel('time step');
pyplot.ylabel('temperature');
pyplot.savefig("TempWatanabeSim.png")
